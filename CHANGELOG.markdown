## 0.1.0.1 (2015-07-23)

Fix compiling with 7.8 (due to the AMP stuff)

## 0.1.0.0 (2015-07-23)

Initial fork from the [dates package](https://hackage.haskell.org/package/dates).
