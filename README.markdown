# Hourglass Fuzzy Parsing

A small package for parsing more human friendly date/time formats into
[hourglass types](https://hackage.haskell.org/package/hourglass). Relative
things like:

* "today", "tomorrow", "yesterday"
* "in 2 days", "3 weeks ago"
* "last monday", "next friday"
* "last month" (1st of this month), "next year" (1st of January of next year)

And absolute things like:

* DD.MM.YYYY
* YYYY/MM/DD
* "12 September 2012 23:12"

4-digits years may be abbreviated (such as 12 for 2012). Both 12-hour and
24-hour time formats are supported, though only for absolute things currently.
Relative times only support offsets of days, weeks, months or years currently.
Only English words and sentence structure are supported.

Originally based on/forked from the [dates package](https://hackage.haskell.org/package/dates).


## TODOs

* Support `<date> at <time>`, e.g., `yesterday at 5 pm`
* Support seconds, minutes, and hours for relative times, e.g., "10 minutes ago"
* Support `<date> on <date>`, e.g., `last week on Tuesday`
* Support `this <date>`, e.g., `this Thursday` meaning the Thursday of "this"
  week (the week "now" currently falls in), regardless of whether it is ahead
  of or behind "now"
* Support `upcoming <date>`, e.g., `upcoming Thursday` or maybe `(this|the)
  upcoming Thursday` meaning the next Thursday when walking forward in time
  (whether that is this week's Thursday or next week's)
* Support `past <date>`, e.g., `past Thursday` or maybe `(this|the)
  past Thursday` meaning the previous Thursday when walking backward in time
  (whether that is this week's Thursday or last week's)
* Write more tests and some benchmarks
