{ mkDerivation, base, hourglass, hspec, parsec, stdenv }:
mkDerivation {
  pname = "hourglass-fuzzy-parsing";
  version = "0.1.0.1";
  src = ./.;
  buildDepends = [ base hourglass parsec ];
  testDepends = [ base hspec ];
  homepage = "https://gitlab.com/doshitan/hourglass-fuzzy-parsing";
  description = "A small library for parsing more human friendly date/time formats";
  license = stdenv.lib.licenses.bsd3;
}
