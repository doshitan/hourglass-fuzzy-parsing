.PHONY: build nix-configure nix-configure-tests test

build:
	cabal build

nix-configure:
	nix-shell --command 'cabal configure'

nix-configure-tests:
	nix-shell --command 'cabal configure --enable-tests'

test:
	cabal test --show-details=always --test-options="--color"
