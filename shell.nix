{ pkgs ? (import <nixpkgs> {})
, haskellPackages ? pkgs.haskell-ng.packages.ghc7101
}:

let
  modifiedHaskellPackages = haskellPackages.override {
    overrides = self: super: {
      hourglass-flexible-parsing = self.callPackage ./. {};
    };
  };
in
  modifiedHaskellPackages.hourglass-flexible-parsing.env
