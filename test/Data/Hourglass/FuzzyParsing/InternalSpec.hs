module Data.Hourglass.FuzzyParsing.InternalSpec where

import Data.Hourglass.FuzzyParsing.Internal
import Test.Hspec

data FuzzyData = Foo | Bar | Baz | Quux deriving (Bounded, Enum, Eq, Show)

main :: IO ()
main = hspec spec

spec :: Spec
spec =
  describe "uniqFuzzyMatch" $ do
    it "should match uniquely for unique partial strings" $ do
      (uniqFuzzyMatch "f" :: Either [FuzzyData] FuzzyData) `shouldBe` Right Foo
      (uniqFuzzyMatch "fo" :: Either [FuzzyData] FuzzyData) `shouldBe` Right Foo
      (uniqFuzzyMatch "bar" :: Either [FuzzyData] FuzzyData) `shouldBe` Right Bar

    it "should match return all non-unique matches when a unique match can not be made" $ do
      (uniqFuzzyMatch "b" :: Either [FuzzyData] FuzzyData) `shouldBe` Left [Bar, Baz]
      (uniqFuzzyMatch "ba" :: Either [FuzzyData] FuzzyData) `shouldBe` Left [Bar, Baz]

    it "should return empty Left if no matches can be made" $
      (uniqFuzzyMatch "c" :: Either [FuzzyData] FuzzyData) `shouldBe` Left []
